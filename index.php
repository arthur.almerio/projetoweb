<?php
session_start();
?>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Inicio</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="public/css/main.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css" integrity="sha384-SI27wrMjH3ZZ89r4o+fGIJtnzkAnFs3E4qz9DIYioCQ5l9Rd/7UAa8DHcaL8jkWt" crossorigin="anonymous">

</head>

<body>
    <audio id="my_audio" src="public/audio/db8bit.mp3" loop="loop"></audio>   

    <div class="goku">
        <img id="gokugif" class="goku-cont" src="public/img/goku12.gif">
    </div>

    <div class="animacao-goku">
        <div class="goku-passando">
            <img src="public/img/goku01.gif">
        </div>
        <div class="goku-rastro">
            <img src="public/img/goku02.gif" class="imagem-rastro" style="height: 64px;">
        </div>
    </div>

    <div class="div-login">
    <h6 id="errolog">Usuário ou senha errados!</h6>
    <form class="form-signin" id="formlogin">
        <h1 class="h3 mb-3 font-weight-normal" style="text-align: center;">Login</h1>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control login" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control login" placeholder="Password" required>
        <button id="submit" class="btn btn-lg btn-light btn-block botao-login" style="width:50%; margin:auto;margin-top:5%;" type="submit">Sign in</button>
    </form>
    </div>



    <footer>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="public/js/main.js"></script>
        <!--
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/js/bootstrap.min.js" integrity="sha384-3qaqj0lc6sV/qpzrc1N5DC6i1VRn/HyX4qdPaiEFbn54VjQBEU341pvjz7Dv3n6P" crossorigin="anonymous"></script>
        -->
    </footer>
</body>

</html>
