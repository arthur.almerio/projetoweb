<?php 
include('controller/counter.php');


	  
$myfile = fopen("counter.txt", "w") or die("Unable to open file!");
fwrite($myfile, $all_value);
fclose($myfile);  
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Menu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="public/css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="public/css/logado.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css" integrity="sha384-SI27wrMjH3ZZ89r4o+fGIJtnzkAnFs3E4qz9DIYioCQ5l9Rd/7UAa8DHcaL8jkWt" crossorigin="anonymous">
    <script src="public/js/logado.js"></script>
</head>

<body>
    <audio id="my_audio" src="public/audio/dbz8bit.mp3" loop="loop"></audio>

    <div class="visitantes">
        <img class="scouter" src="public/img/scouter.png">
        <div class="counter">
            <h2><?php echo $all_value; ?> </h2>
        </div>
    </div>
    
    <div class="upload">
        <h1>Faça Upload de Imagem ou GIF<h1>
        <form action="controller/upload.php" method="POST" enctype="multipart/form-data">
            <input type="file" name="fileUpload">
            <input type="submit" value="Enviar">
        </form>
    </div>
    
    <div class="goku-luta">
        <img src="public/img/goku21.gif">
    </div>

    <footer>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/js/bootstrap.min.js" integrity="sha384-3qaqj0lc6sV/qpzrc1N5DC6i1VRn/HyX4qdPaiEFbn54VjQBEU341pvjz7Dv3n6P" crossorigin="anonymous"></script>
    </footer>
</body>

</html>