$(document).ready(function(){
    $('#my_audio')[0].play();
	$('#errolog').hide(); //Esconde o elemento com id errolog
	$('#formlogin').submit(function(){ 	//Ao submeter formulário
		var login=$('#inputEmail').val();	//Pega valor do campo email
        var senha=$('#inputPassword').val();	//Pega valor do campo senha
        console.log(login,senha);
		$.ajax({			//Função AJAX
			url:"controller/login.php",			//Arquivo php
			type:"post",				//Método de envio
			data: "login="+login+"&senha="+senha,	//Dados
   			success: function (result){		//Sucesso no AJAX
                		if(result == 1){						
                			animacao();	//Redireciona
                		}else{
                			$('#errolog').show();		//Informa o erro
                		}
            		}
		})
		return false;	//Evita que a página seja atualizada
    })

    function animacao() {
        tempo = 50;
        posicao = -10;
        setTimeout(incrementa, tempo);
        $(".div-login").fadeOut(3500);
    
        function incrementa() {
            posicao += 1;
            margin = posicao+'%';
            $( ".goku-passando" ).css('margin-left', posicao+'%');
            $('.imagem-rastro').css('width', posicao+'%');
    
            if (posicao <= 110) {
                setTimeout(incrementa, tempo);
            } else {
                $(".goku-passando").css('margin-left', '-10%');
                $('.imagem-rastro').css('width', '0%');
                window.location.href = "/walter2/menu.php";
            }
        }
    }
});


